#include "robotaudioplayback.h"

SkApp *skApp = nullptr;

int main(int argc, char *argv[])
{
    skApp = new SkApp(argc, argv);

    SkCli *cli = skApp->appCli();

    cli->add("--src-channel",   "-c", "",   "Setup the source channel");
    cli->add("--show-devices",  "-l", "",   "Show audio devices and exit");

    if (cli->check())
    {
        if (cli->isUsed("--show-devices"))
        {
            logger->enable(false);
            SkStringList l = RobotAudioPlayback::getDevicesNames();

            cout << "\n\nAudio devices:" << "\n";

            for(ULong i=0; i<l.count(); i++)
                cout << " -> (" << i << ") " << l[i] << "\n";

            exit(0);
        }
    }

#if defined(ENABLE_HTTP)
    SkFlowSat::addHttpCLI();
#endif

    skApp->init(5000, 150000, SK_TIMEDLOOP_RT);
    new RobotAudioPlayback;

    return skApp->exec();
}
