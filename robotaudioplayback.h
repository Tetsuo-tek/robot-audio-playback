#ifndef ROBOTAUDIOPLAYBACK_H
#define ROBOTAUDIOPLAYBACK_H

#if defined(ENABLE_AUDIO) && defined(ENABLE_PORTAUDIOLIB)

#include <Core/System/Network/FlowNetwork/skflowsat.h>
#include <Core/System/Network/FlowNetwork/skflowaudiosubscriber.h>
#include <Multimedia/Audio/PA/skportaudio.h>

class RobotAudioPlayback extends SkFlowSat
{
    SkFlowAudioSubscriber *subscriber;
    SkString srcChanName;
    SkFlowChannel *srcChan;

    SkString device;
    SkPortAudio *pa;
    SkAudioFmtType paSampleType;
    SkAudioOutput *paOutput;

    public:
        Constructor(RobotAudioPlayback, SkFlowSat);

        Slot(onSubscriberReady);

        static SkStringList getDevicesNames();

    private:
        bool onSetup()                                  override;
        void onInit()                                   override;
        void onQuit()                                   override;

        void onChannelAdded(SkFlowChanID chanID)        override;
        void onChannelRemoved(SkFlowChanID chanID)      override;

        //void onFlowDataCome(SkFlowChannelData &chData)  override;

};

#endif

#endif // ROBOTAUDIOPLAYBACK_H
