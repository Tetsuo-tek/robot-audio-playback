#include "robotaudioplayback.h"

#if defined(ENABLE_AUDIO) && defined(ENABLE_PORTAUDIOLIB)

#include <Core/Containers/skarraycast.h>

ConstructorImpl(RobotAudioPlayback, SkFlowSat)
{
    srcChan = nullptr;
    subscriber = nullptr;
    pa = nullptr;
    paOutput = nullptr;

    setObjectName("RobotAudioPlayback");

    SlotSet(onSubscriberReady);
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

bool RobotAudioPlayback::onSetup()
{
    SkCli *cli = skApp->appCli();
    AssertKiller(!cli->isUsed("--src-channel"));
    srcChanName = cli->value("--src-channel").toString();

    paSampleType = AFMT_FLOAT;
    device = "default";

    subscriber = new SkFlowAudioSubscriber(this);
    Attach(subscriber, ready, this, onSubscriberReady, SkOneShotDirect);
    setupSubscriber(subscriber);

    return true;
}

void RobotAudioPlayback::onInit()
{
}

void RobotAudioPlayback::onQuit()
{
    if (!pa)
        return;

    subscriber->audioBuffer().delOutput("PaInt");
    paOutput = nullptr;

    pa->stop();
    pa->close();
    pa->destroyLater();
    pa = nullptr;

    device.clear();
}

void RobotAudioPlayback::onChannelAdded(SkFlowChanID chanID)
{
    SkFlowChannel *ch = channel(chanID);
    AssertKiller(!ch);

    if (ch->name == srcChanName)
    {
        srcChan = ch;
        subscriber->subscribe(ch->name.c_str());
    }
}

void RobotAudioPlayback::onChannelRemoved(SkFlowChanID chanID)
{
    SkFlowChannel *ch = channel(chanID);
    AssertKiller(!ch);

    if (ch->name == srcChanName)
    {
        srcChan = nullptr;
        quit();
    }
}

/*void RobotAudioPlayback::onFlowDataCome(SkFlowChannelData &chData)
{
    if (!subscriber->isReady())
        return;

    subscriber->tick(chData);
}*/

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

SlotImpl(RobotAudioPlayback, onSubscriberReady)
{
    SilentSlotArgsWarning();

    pa = new SkPortAudio(this);
    pa->setObjectName(this, "PortAudio");

    AssertKiller(!pa->init());

    SkAudioBuffer &a = subscriber->audioBuffer();
    SkAudioParameters &p = subscriber->parameters();
    SkAudioParameters paParams(false, p.getChannels(), p.getSampleRate(), p.getBufferFrames(), paSampleType);
    a.addOutput("PaInt", paParams, pa->getProduction().outputProduction, true);

    AssertKiller(!pa->play(device.c_str(), paParams));
    paOutput = a.getOutputByName("PaInt");
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

SkStringList RobotAudioPlayback::getDevicesNames()
{
    SkPortAudio *pa = new SkPortAudio;

    AssertKiller(!pa->init());
    SkStringList &devList = pa->getDevicesNames();
    pa->close();
    pa->destroyLater();//it is asynch

    return devList;
}

#endif
